//Native imports
const process = require('process'); //Handling/causing process events
const net = require('net'); //For Socket communications
const fs = require('fs');//filesystem

//Required, external imports
const sni = require("sni"); //For getting domain of https requests
const yargs = require('yargs')(process.argv.slice(2)); //For command-line arguments/flags

//Setting up command line arguments
var argv = yargs
	.count('verbose')
	.alias('v', 'verbose')
	//.option('watch', {alias: 'w',description: 'Tells whether or not to run the status module for health updates',type: 'boolean',})
    .option('https', {alias: 's',description: 'Which port on the container to listen for https on',type: 'number',nargs:1,default:443,})
	.option('http', {alias: 'h',description: 'Which port on the container to listen for http on',type: 'number',nargs:1,default:80,})
	.option('run', {alias: 'r',description: 'run program from command line',type: 'boolean'/*DEFAULTS TO FALSE*/})
	.option('onlysecure', {alias: 'o',description: 'run https only',type: 'boolean'/*DEFAULTS TO FALSE*/})
	.option('nosecure', {alias: 'n',description: 'run http only',type: 'boolean'/*DEFAULTS TO FALSE*/})
	.option('file', {alias: 'f',description: 'Directory from proxy dir to json file of servers', type: 'string',nargs:1,default:"./servers.json"})//FOR TESTING ONLY----------------------------
	.option('test', {alias: 't',description: 'For use in testing only', type: 'boolean',})//FOR TESTING ONLY----------------------------
    .argv;

//Setup verbosity functions for info/warnings/debugging/logging
function WARN() {argv.verbose >= 0 && console.log.apply(console,arguments);}
function INFO() {argv.verbose >= 1 && console.log.apply(console,arguments);}
function DEBUG(){argv.verbose >= 2 && console.log.apply(console,arguments);}


//Setting up program kill listeners...
//process.on('SIGKILL',()=>{process.exit(0);});//Redundant listener (Cannot add) for Immediate process killing
process.on('SIGTERM',()=>{process.exit(0);});//Kills from terminal (or docker stop)
process.on('SIGINT',()=>{process.exit(0);});//Listen to kill program on interruption (like Ctrl+c in terminal)

//Function to check if a port is theoretically useable
function portPreRegistered(port,bypass=false){//--------------------------------------------------------------------MAY WANT TO MODIFY-----------------------------
	if([22,23,80,443].includes(port)||(port>49151&&port<65536)){return false;}
	else{
		DEBUG("Use of pre-registered port:"+port+" detected but bypassed as directed in function portPreRegistered()");
		return (!(bypass));
	}
}
//Set ports to either set or get correct ports http/s 80/443 are defaults
var httpPort = ()=>{
	if(arguments.length>0){argv.h = arguments[0];}
	return argv.h;
};

var httpsPort = ()=>{
	if(arguments.length>0){argv.s = arguments[0];}
	return argv.s;
};

//Create maps for all proxies and endpoint servers
let proxies = new Map();
let servers = new Map();
//register servers into servers map, or die trying------------------------------------------------------------------------------------

//function to set up a proxy server
function setupProxyServer(listenPort,proxyName){
	//Create server
	let server = net.createServer();
	//When a connection is established to the client
	server.on('connection', (clientToProxySocket)=>{
		DEBUG('Client Connected To Proxy: '+proxyName);
		//console.log('Client IP? '+clientToProxySocket.remoteAddress+"   "+clientToProxySocket.address().address);//---------------------------------------------
		// We need only the starting packet (hence .once)
		clientToProxySocket.once('data', (data) => {
			//Check if connection is TLS (https)
			let isTLSConnection = (sni(data)!=null);
			let isEncryptedConnection = isTLSConnection;//------------MAYBE SWITCH VARIABLE NAME TO THIS ------------------------------------------
			
			//get default port for the protocol
			let protocolDefaultPort = (!(isTLSConnection) ? httpPort() : httpsPort());//if connection is not encrypted use http port, otherwise https
			
			var serverAddress;
			// Find domain if https
			if(isTLSConnection){serverAddress = sni(data);}
			//Find domain if http
			else{
				serverAddress = data.toString().split('Host: ')[1].split('\r\n')[0];
			}
			
			//Get short address (TLD)
			let shortAddr = serverAddress.split('.');
			//Account for localhost
			if(serverAddress==='localhost'||serverAddress=='127.0.0.1'){shortAddr = 'localhost';}
			
			else{
				//IF BELOW DOES NOT WORK TRY THIS //if(shortAddr.length==1){DEBUG("Single-length short address detected: "+shortAddr[0]);shortAddr = shortAddr[0];}//-------------------------------------------------------------
				if(shortAddr.length==1){DEBUG("Single-length short address detected: "+shortAddr[0]);shortAddr = serverAddress;}
				else{shortAddr = shortAddr[shortAddr.length-2]+shortAddr[shortAddr.length-1];}
				//IF ABOVE DOES NOT WORK TRY THIS //else{shortAddr = shortAddr.slice(0,-1).join("");}
				//ORIGINAL of above, MAY HAVE BROKEN THE PROGRAM SOMEHOW =>////-------------------------------------------------------------
				//shortAddr = (shortAddr.length==1 ? shortAddr[1] : shortAddr[shortAddr.length-2]+shortAddr[shortAddr.length-1]);//-------------------------------------------------------------
			}
		
		
			/////////////////////////////////////////////////////////////////////////////////CURRENT STOP MARK IN LOOKTHROUGH////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////CURRENT STOP MARK IN LOOKTHROUGH////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////CURRENT STOP MARK IN LOOKTHROUGH////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////CURRENT STOP MARK IN LOOKTHROUGH////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////CURRENT STOP MARK IN LOOKTHROUGH////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////CURRENT STOP MARK IN LOOKTHROUGH////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////CURRENT STOP MARK IN LOOKTHROUGH////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////CURRENT STOP MARK IN LOOKTHROUGH////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////CURRENT STOP MARK IN LOOKTHROUGH////////////////////////////////////////////////////////////////////////
			
			//If the host exists in record, Create a connection to the https server 
			if(servers.has(shortAddr)){
				var portTo;
				let serverTo = servers.get(shortAddr);
				
				//If ports are not defined for that server, fall back to defaults
				if(!('ports' in serverTo)){portTo = protocolDefaultPort;}
				
				//If ports are defined, assign value to portTo, even if default
				else{var protocolStr = (isTLSConnection ? 'https' : 'http'); //string denoting used protocol
					portTo = ((protocolStr in serverTo.ports) ? serverTo.ports[protocolStr] : protocolDefaultPort);
				}
				
				//Create a connection to the server 
				let proxyToServerSocket = net.createConnection({host:shortAddr,port:portTo},()=>{
					/*IGNORE THIS LINE*///clientToProxySocket.write('HTTP/1.1 200 OK\r\n\n');//clientToProxySocket.write("HTTP/1.1 200 Connection established\r\n\r\n");
					//clientToProxySocket.write('HTTP/1.1 404 Not Found\r\n\n');
					//Forward initial request to server
					proxyToServerSocket.write(data);
					//Pipe client connection to server and vise-versa (create bi-directional communication between client and server)
					clientToProxySocket.pipe(proxyToServerSocket);
					proxyToServerSocket.pipe(clientToProxySocket);
				});
				
				//Handle errors between proxy and server
				proxyToServerSocket.on('error',(err)=>{
					WARN('Error between proxy ('+proxyName+') and host ('+serverTo+':'+portTo+')...');
					DEBUG(err);
				});
			}
			//Request was made to an to unknown server, destroy connection
			else{
				WARN('Proxy ('+proxyName+'): request made to unknown server ('+shortAddr+')');
				clientToProxySocket.destroy();
			}
			
			//Handle errors between proxy and client (ignore but allow debugging)
			clientToProxySocket.on('error',(err)=>{
				INFO('Proxy ('+proxyName+') ignored error in client connection, see debug for stacktrace');
				DEBUG(err);
			});
		});
	});

	//Handle proxy server errors
	server.on('error',(err)=>{//nonfatal per https://nodejs.org/api/net.html#net_event_error
		WARN('SERVER ERROR: not fatal (hopefully)...');
		INFO(err);//Stacktrace in warn
	});

	//When server closes
	server.on('close', ()=>{
		DEBUG('Proxy server ('+proxyName+') closed!');//ENSURE THIS IS CORRECT, IS IT THE SERVER CLOSING OR THE SOCKET CONNECTION----------------------------
		process.exit(0);
	});

	//Set proxy server to listen on specified port
	server.listen(listenPort,()=>{
		WARN('Proxy server ('+proxyName+') running on port:'+listenPort);
	});
	
	return server;
}

//Constructor for customError
function customError(errType,errData){return new Error({type:errType,data:errData});}


//Register servers from an array of 
function registerServersJSON(json){
	for(var i=0;i<json.length;i++){
		try{registerServer(json[i],i);}
		catch(registerError){WARN(registerError);}
	};
}

//Function to register servers
function registerServersFile(pathToServersJson){
	if(fs.existsSync(pathToServersJson)){//If file exists
		try{
			let fileContents = fs.readFileSync(pathToServersJson);//Read file
			let tempServ = JSON.parse(fileContents);//Parse file as json
			//register each server, or notify on error
			registerServersJSON(tempServ);
			return true;
		}
		catch(jsonErr){
			WARN("Error reading or parsing registerServers(path) JSON file!");
			DEBUG(jsonErr);
			return false;
		}
	}else{
		WARN("Error: path: "+pathToServersJson+" in registerServers(path) is not a JSON file containing server info!");
		return false;
	}	
}


//Function to register a single server
function registerServer(serverJson,identifier){
	if('host' in serverJson){
		if('ports' in serverJson){
			//Ensure ports are in correct format, range, and/or useable
			const isNumeric = (str)=>{if(typeof str != "string"){return false;}return !isNaN(str) && !isNaN(parseFloat(str));};//Checks if a string is numeric
			for(const portName in serverJson.ports){
				if(typeof serverJson.ports[portName]!='number'){
					if(typeof serverJson.ports[portName]!='string'){
						WARN("In registerServer for id: "+identifier+", obj.ports."+portName+" needs to be a number 123 or string '123', currently: ("+(typeof serverJson.ports[portName])+"). skipping port");
						delete serverJson.ports[portName];
					}else{//If is a string
						if(isNumeric(serverJson.ports[portName])){serverJson.ports[portName]=Number(serverJson.ports[portName]);}//if Is numeric turn into a number
						else{//If non-numeric string, delete port entry
							WARN("In registerServer for id: "+identifier+", obj.ports."+portName+" is non-numeric string: '"+(typeof serverJson.ports[portName])+"'. (ex: 123 or '123') skipping port");
							delete serverJson.ports[portName];
						}
					}
				}
			}
			//set value for host if ports has any keys
			if(Object.keys(serverJson.ports).length>0){
				//If entry exists, warn of overwrite
				if(servers.has(serverJson.host)){
					WARN('Overwriting host:'+serverJson.host);
					DEBUG(servers.get(serverJson.host),serverJson.host);
				}//Write to servers registry
				servers.set(serverJson.host,{ports:serverJson.ports});
			}
			//Otherwise throw error
			else{throw customError('noValidPorts',serverJson.ports);}
		}else{throw customError('noPorts',serverJson);}//if obj.host.ports does not exist, skip and warn
	}else{throw customError('noHost',serverJson);}//if obj.host does not exist, skip and warn
}

//function to start a proxy
function startProxy(name,portFromCli){//----------------------------------------------------------------------- Ensure port ok???--------------------------------------
	if(proxies.has(name)){
		if(!(proxies.get(name).server.destroy().destroyed)){
			throw customError('proxy_not_destroyed','Unable to destroy proxy');
		}
	}
	proxies.set(name,{port:portFromCli,server:setupProxyServer(portFromCli,name)});
}

function stopProxyByPort(portt){
	proxies.forEach((info,name)=>{
		if(info.port==portt){
			if(proxies.get(name).server.destroy().destroyed){
				return proxies.delete(name);//returns true/false
			}else{return false;}
		}
	});
	return false;
}

function stopProxyByName(name){
	if(proxies.has(name)){
		return proxies.get(name).destroy().destroyed;
	}else{return false;}
}

function stopProxy(val){
	if(typeof val === 'number'){return stopProxyByPort(val);}
	else{
		if(typeof val === 'string'){return stopProxyByName(val);}
		else{return false;}
	}
}

//SETTING UP EXPORTS
exports.serversFile = (fil)=>{//Tries to register servers, returns true if successful, false otherwise
	argv.f = fil;
	return registerServersFile(fil);
};
exports.serversJSON = registerServersJSON;
exports.proxyHttpsPort = (p)=>{argv.s=p;return p;};
exports.proxyHttpPort = (p)=>{argv.h=p;return p;};
exports.test = (options={})=>{argv.t = true;runTests(options);};
exports.startProxy = startProxy;
exports.stopProxy = stopProxy;
exports.stopProxyByName = stopProxyByName;
exports.stopProxyByPort = stopProxyByPort;
exports.getProxyByName = (name)=>{if(proxies.has(name)){return proxies.get(name);}else{throw customError('noProxyFound',name);}};
exports.getProxyByPort = (p)=>{
	proxies.forEach((info,name)=>{
		if(info.port==p){return info;}
	});
	throw customError("noProxyFound",p);
};
exports.servers = servers;


//Function to run tests (SHOULD BE AT VERY BOTTOM OF SCRIPT)
function runTests(options={}){// options.(http(s)(Proxy/Server)Port,)
	if(registerServersFile(argv.f)){
		const test = require('./testing/testServer.js');
		test.setupHttp(('host' in options ? options.host : 'localhost'),('httpServerPort' in options ? options.httpServerPort : 8000));
		test.setupHttps(('host' in options ? options.host : 'localhost'),('httpsServerPort' in options ? options.httpsServerPort : 8001));
		startProxy('proxyHttps',('httpsProxyPort' in options ? options.httpsProxyPort : 443));
		startProxy('proxyHttp',('httpProxyPort' in options ? options.httpProxyPort : 80));
	}else{WARN("Error registering servers");}
}
if(argv.t){runTests();}//Test defaults if -t or -r flag is present

//If run but not test, run http and https with current args
if(argv.r&&!(argv.t)){
	if(!argv.n){startProxy('proxyHttps',argv.s);}//If not nonsecure, start https
	if(!argv.o){startProxy('proxyHttp',argv.h);}//If not onlysecure, start http
}